#include "main.h"
#include "sha256.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <future>

int main()
{
    miner *Miner = new miner();
    delete Miner;
    return 0;
}

miner::~miner()
{
    delete Network;
}

miner::miner()
{
    Network = new network("http://6857coin.csail.mit.edu:8080");
    running = true;
    std::vector<std::future<uint64_t>> threads;
    for(unsigned int i = 0; i < std::thread::hardware_concurrency(); i++)
    {
        threads.push_back(std::async(&miner::mainThread, this));

    }
    threads[0].get();
}

Json::Value miner::minerThread(Json::Value header, uint64_t start = 0, uint64_t jobsize = 20000000000)
{
    SHA256 sha256;
    std::string root = sha256("jamesl22");
    header["root"] = root;

    struct solutions
    {
        uint64_t nonce[3];
    };
    std::map<uint64_t, solutions> nonces;

    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    uint64_t time = std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count();

    uint64_t nonce = 0;
    uint8_t version = 0;
    uint64_t difficulty = header["difficulty"].asUInt64();

    Json::Value block;
    block["header"] = header;
    block["block"] = "jamesl22";
    block["header"]["timestamp"] = static_cast<Json::UInt64>(time);

    for(nonce = start; nonce < jobsize; nonce++)
    {
        std::stringstream concat;
        concat << header["parentid"].asString() << header["root"].asString() << int_to_hex(difficulty) << int_to_hex(time) << int_to_hex(nonce) << int_to_hex(version);

        std::string hash = sha256(concat.str());

        std::vector<unsigned char> bytes = HexToBytes(hash);

        uint64_t hashint = *((uint64_t*)bytes.data());
        uint64_t mod = hashint % uint64_t(2^difficulty);

        solutions Solution;
        Solution.nonce[0] = nonce;
        std::pair<std::map<uint64_t, solutions>::iterator, bool> ret;
        ret = nonces.insert(std::pair<uint64_t, solutions>(mod, Solution));
        if(!ret.second)
        {
            //std::cout << " " << header["parentid"].asString() << " " << header["root"].asString() << " " << int_to_hex(difficulty) << " " << int_to_hex(time) << " " << int_to_hex(nonce) << " " << int_to_hex(version);
            //std::cout << concat.str() << std::endl;
            Solution = ret.first->second;
            if(Solution.nonce[1] == 0)
            {
                ret.first->second.nonce[1] = nonce;
            }
            else if (Solution.nonce[2] == 0)
            {
                ret.first->second.nonce[2] = nonce;
            }
            else
            {
                block["header"]["nonces"][0] = static_cast<Json::UInt64>(ret.first->second.nonce[0]);
                block["header"]["nonces"][1] = static_cast<Json::UInt64>(ret.first->second.nonce[1]);
                block["header"]["nonces"][2] = static_cast<Json::UInt64>(ret.first->second.nonce[2]);
                block["hashes"] = static_cast<Json::UInt64>(nonce - start);
                return block;
            }
        }
    }

    return block;
}

uint64_t miner::mainThread()
{
    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    uint64_t time1 = std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count();

    Json::Value block = minerThread(Network->getHeader());

    now = std::chrono::system_clock::now();
    duration = now.time_since_epoch();
    uint64_t time2 = std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count();

    Network->submitBlock(block);

    return block["hashes"].asUInt64()/((time2 - time1)*10^12);
}

network::network(std::string url)
{
    this->url = url;
}

network::~network()
{

}

Json::Value network::getHeader()
{
    RestClient::Response r = RestClient::get(this->url + "/next");
    Json::Value header;
    Json::CharReaderBuilder rbuilder;
    rbuilder["collectComments"] = false;
    std::string errs;
    std::stringstream input;
    input << r.body;
    Json::parseFromStream(rbuilder, input, &header, &errs);
    return header;
}

bool network::submitBlock(Json::Value block)
{
    Json::StreamWriterBuilder wbuilder;
    wbuilder["indentation"] = "\t";
    std::string response = Json::writeString(wbuilder, block);
    RestClient::Response r = RestClient::post(this->url + "/add", "text/json", response);
    std::cout << "Submitting block: " << response << std::endl;
    std::cout << r.body << std::endl;
    return true;
}
