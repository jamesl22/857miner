#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <string>
#include <sstream>
#include <iomanip>
#include <algorithm>

#include <restclient-cpp/restclient.h>
#include <jsoncpp/json/value.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/reader.h>

std::vector<unsigned char> HexToBytes(const std::string& hex)
{
    std::vector<unsigned char> bytes;

    for (unsigned int i = 0; i < hex.length(); i += 2)
    {
        std::string byteString = hex.substr(i, 2);
        unsigned char byte = (unsigned char) strtol(byteString.c_str(), NULL, 16);
        bytes.push_back(byte);
    }

    return bytes;
}

template< typename T >
std::vector<unsigned char> intToBytes(T num)
{
     std::vector<unsigned char> arrayOfBytes;
     for(unsigned int i = sizeof(T) - 1; i >= 0; i--)
     {
         arrayOfBytes.push_back(num>>(8*i));
     }
     return arrayOfBytes;
}

template< typename T >
std::string int_to_hex( T i )
{
    std::stringstream stream;
    if(sizeof(T) == 1)
    {
        stream << std::setfill ('0') << std::setw(3)
               << std::hex << i;
    }
    else
    {
        stream << std::setfill ('0') << std::setw(sizeof(T)*2)
               << std::hex << i;
    }
    std::string toReverse = stream.str();
    //std::reverse(toReverse.begin(), toReverse.end());
    return toReverse;
}

std::string GetBinaryStringFromHexString (std::string sHex)
{
    std::string sReturn = "";
    for (unsigned int i = 0; i < sHex.length (); ++i)
    {
        switch (sHex [i])
        {
        case '0':
            sReturn.append ("0000");
            break;
        case '1':
            sReturn.append ("0001");
            break;
        case '2':
            sReturn.append ("0010");
            break;
        case '3':
            sReturn.append ("0011");
            break;
        case '4':
            sReturn.append ("0100");
            break;
        case '5':
            sReturn.append ("0101");
            break;
        case '6':
            sReturn.append ("0110");
            break;
        case '7':
            sReturn.append ("0111");
            break;
        case '8':
            sReturn.append ("1000");
            break;
        case '9':
            sReturn.append ("1001");
            break;
        case 'a':
            sReturn.append ("1010");
            break;
        case 'b':
            sReturn.append ("1011");
            break;
        case 'c':
            sReturn.append ("1100");
            break;
        case 'd':
            sReturn.append ("1101");
            break;
        case 'e':
            sReturn.append ("1110");
            break;
        case 'f':
            sReturn.append ("1111");
            break;
        }
    }
    return sReturn;
}


template <typename T>
std::string tostring(const T& t)
{
    std::ostringstream ss;
    ss << t;
    return ss.str();
}


class network
{
public:
    network(std::string url);
    ~network();
    Json::Value getHeader();
    bool submitBlock(Json::Value block);

private:
    std::string url;
};

class miner
{
public:
    miner();
    ~miner();

private:
    network *Network;
    uint64_t mainThread();
    bool running = false;
    Json::Value minerThread(Json::Value header, uint64_t start, uint64_t jobsize);
    struct uint256_t
    {
        uint64_t bits[4];
    };

};

//RestClient::Response r = RestClient::get("http://url.com")
//RestClient::Response r = RestClient::post("http://url.com/post", "text/json", "{\"foo\": \"bla\"}")

#endif // MAIN_H_INCLUDED
